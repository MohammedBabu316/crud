# CRUD

A crud table with backend api implemented.

## Installation

Make sure you have [Node.js](http://nodejs.org/) and [Npm](https://www.npmjs.com/) installed.

To run backend you need to do following in your command line in the root directory of the project.
```sh
cd api
npm install
npm start
```

This will run your backend on port 3232 and installs required dependencies to the package.json file.

## Usage

Now open the **'crud_table.html'** file in the chrome web browser and check the crud operations.

NOTE: click on the **'Load Table'** button to initially fetch the data from api.